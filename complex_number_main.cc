// complex_number_main.cc

#include <iostream>
#include <string>
#include "complex_number.h"

using namespace std;

bool ProcessEquation() {
  double real, imag;
  cin >> real >> imag;
  ComplexNumber c(real, imag), d;
  while (true) {
    string op;
    cin >> op;
    if (op == "+" || op == "-" || op == "*" || op == "/") {
      cin >> real >> imag;
      d.Set(real, imag);
      if (op == "+") c.Copy(c.Add(d));
      else if (op == "-") c.Copy(c.Subtract(d));
      else if (op == "*") c.Copy(c.Multiply(d));
      else if (op == "/") c.Copy(c.Divide(d));
    } else if (op == "=") {
      cout << c.real() << " + " << c.imag() << "i" << endl;
      return true;
    } else {
      return false;
    }
  }
}

int main() {
  while (ProcessEquation()) {
    // Nothing else to do.
  }
  return 0;
}

